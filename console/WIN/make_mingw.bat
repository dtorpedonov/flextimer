@echo off

cd ..
if not exist build_mingw mkdir build_mingw
cd build_mingw

setlocal EnableDelayedExpansion

set CygPath=0
for /f "delims=" %%a in ('cygpath -w -p /bin 2^>nul') do @set CygPath=%%a
if not %CygPath%==0 path=!path:%CygPath%;=!

cmake -G "MinGW Makefiles" ..
mingw32-make.exe
