#if !defined TIMERCORE_H
#define TIMERCORE_H

#include <boost/chrono.hpp>
#include <boost/shared_ptr.hpp>
#include <cmath>
#include <vector>
#include "base_iface.h"
#include "stimer.h"

typedef boost::chrono::duration<double> time_interval;
typedef boost::chrono::steady_clock::time_point time_point; 

typedef std::vector<single_timer_pointer>::iterator timer_iterator;

//--------------------------------------------------------------------------------
class kernel
{
  public:
    kernel(void) : state(IDLE), stimer_idx(0), 
      another_timer_flag(true), iface_ptr() {}
    
    typedef enum { IDLE, INIT, PROGRESS, FINISH } timer_state;
    
    void execute(void);
    
    void add_interface(base_iface_pointer some_iface_ptr)
    { iface_ptr = some_iface_ptr; }
    
    timer_state get_state(void) { return state; }
    time_interval get_timer_time(void) { return stimers[stimer_idx]->get_time(); }
    
    void set_timer(int max_counter_) { stimers.back()->set_timer(max_counter_); }
    void set_repeat(int repeat_) { stimers.back()->set_repeat(repeat_); }
    void set_another(bool flag_) { this->another_timer_flag = flag_; }
    
  protected:
    timer_state state;
    
    std::vector<single_timer_pointer> stimers;
    int stimer_idx;
    
    bool another_timer_flag;
    
    time_point get_now(void);
    time_interval get_seconds(long);
    
    void init_all_timers();
    void run_all_timers();
    
    base_iface_pointer iface_ptr;
    
    // service methods
    void ask_iface_for_next_timer(void) {
      this->another_timer_flag = false;
      iface_ptr->notify();
    }
    
    void iface_display_time(void) { iface_ptr->notify(); }
    void iface_display_finish(void) { iface_ptr->notify(); }
    
    bool init_next_timer(void) { return this->another_timer_flag; }
};

typedef boost::shared_ptr<kernel> kernel_pointer;

#endif // TIMERCORE_H
