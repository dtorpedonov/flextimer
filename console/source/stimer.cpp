#include <iostream>

#include <iostream>
#include "stimer.h"

using namespace std;

//--------------------------------------------------------------------------------
time_point single_timer::get_now(void) { return boost::chrono::steady_clock::now(); } 
time_interval single_timer::to_seconds(long secs) { return boost::chrono::seconds(secs); }

//--------------------------------------------------------------------------------
void single_timer::start(void)
{
  this->current_sec = this->to_seconds(0);
  this->start_time = this->get_now();
}

//--------------------------------------------------------------------------------
bool single_timer::is_finished(void)
{
  bool ret = false;
  
  time_interval delta = this->get_now() - start_time;
  
  while (delta < current_sec) { delta = this->get_now() - start_time; }
  
  if (delta >= this->to_seconds(max_counter)) { // TODO: calculate time precise (+/-0.5 now)
    iteration++;
    ret = true;
  }
  
  this->current_sec++;
  
  return ret;
}
