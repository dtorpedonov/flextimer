#if !defined KERNEL_IFACE_H
#define KERNEL_IFACE_H

#include <boost/shared_ptr.hpp>

#include "base_iface.h"
#include "kernel.h"

class kernel_iface : public base_iface
{
  public:

    virtual void set_kernel(kernel_pointer krn_ptr)
    { kernel_ptr = krn_ptr; }
      
  protected:
    kernel_pointer kernel_ptr;
};

typedef boost::shared_ptr<kernel_iface> kernel_iface_pointer;

#endif // KERNEL_IFACE_H
