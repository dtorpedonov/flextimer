#include <iostream>

#include "stimer.h"
#include "kernel.h"
#include "console_iface.h"

//==============================================================================
void kernel::execute(void)
{
  init_all_timers();
  run_all_timers();
  return;
}

//--------------------------------------------------------------------------------
void kernel::init_all_timers(void)
{
  this->state = INIT;
  
  while (this->init_next_timer()) { // another_timer_flag inside - not good
    single_timer_pointer one_timer(new single_timer);
    stimers.push_back(one_timer);
    
    this->ask_iface_for_next_timer(); // another)timer_flag inside -- not good
  }
}

//--------------------------------------------------------------------------------
void kernel::run_all_timers(void)
{
  for (timer_iterator it = stimers.begin();  it < stimers.end(); ++it) {
    
    while( (*it)->check_next_iteration() ) {
      
      this->state = PROGRESS;
      (*it)->start();
      
      while ( (*it)->is_finished() == false) {
        this->iface_display_time();
      }
      
      this->state = FINISH;
      this->iface_display_finish();
    }
    stimer_idx++;
  }
}

