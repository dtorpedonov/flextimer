#if !defined CONSOLE_INTERFACE_2_H
#define CONSOLE_INTERFACE_2_H

#include <boost/shared_ptr.hpp>

#include "kernel_iface.h"
#include "kernel.h"


class console_iface_2 : public kernel_iface
{
  public:
    console_iface_2(void) {}

    virtual void notify(void);
    
  private:
    void display_header(void);
    void display_counter(time_interval actual_time);
    void display_finish(void);
    int  get_timer_counter(void);
    int  get_repeat(void);
    bool check_another(void);
};

#endif // CONSOLE_INTERFACE_2_H
