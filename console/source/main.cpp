#include <iostream>

#include "console_package.h"

using namespace std;


int main(void)
{
  console_package package;
  package.execute();
  
  return 0;
}

