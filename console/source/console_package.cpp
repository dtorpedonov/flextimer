#include "console_package.h"

void console_package::execute(void)
{
  this->iface_ptr->set_kernel( this->kernel_ptr );
  this->kernel_ptr->add_interface(this->iface_ptr);
  
  this->kernel_ptr->execute();
  
}
