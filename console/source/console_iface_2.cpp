#include <string>
#include <iostream>

#include "console_iface_2.h"
#include "kernel.h"

using namespace std;

void console_iface_2::notify(void)
{
  kernel::timer_state state = this->kernel_ptr->get_state();
  
  if (state == kernel::INIT) { 
    this->display_header(); 
    
    int counter = this->get_timer_counter();
    this->kernel_ptr->set_timer(counter);
    
    counter = this->get_repeat();
    this->kernel_ptr->set_repeat(counter);
    
    bool check = this->check_another();
    this->kernel_ptr->set_another(check);
  }
  
  if (state == kernel::PROGRESS) {
    this->display_counter( this->kernel_ptr->get_timer_time());
  }
  
  if (state == kernel::FINISH) {
    this->display_finish();
  }
}


void console_iface_2::display_header()
{ cout << "start" << endl; }

void console_iface_2::display_counter(time_interval actual_time)
{ cout << '\r' << actual_time << flush; }

void console_iface_2::display_finish(void)
//{ cout << endl << "finish\a\a\a\a\a" << endl; }
{ cout << endl << "finish" << endl; }

int console_iface_2::get_timer_counter(void)
{
  string counter;
  cout << "enter the timer counter: ";
  cin >> counter;
  return atoi(counter.c_str());
}

int console_iface_2::get_repeat(void)
{
  string counter;
  cout << "enter the iteration number: ";
  cin >> counter;
  return atoi(counter.c_str());
}

bool console_iface_2::check_another(void)
{
  string check;
  cout << "another timer? ";
  cin >> check;
  
  if (check == "Y" || check == "y" || check == "1" ) { return true;}
  return false;
}
