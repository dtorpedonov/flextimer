#if !defined CONSOLE_PCKG_H
#define CONSOLE_PCKG_H

#include "kernel.h"
#include "console_iface_2.h"
#include "console_iface.h"
#include "file_iface.h"


class console_package
{
  public:
    console_package(void) : kernel_ptr(new kernel),
    //iface_ptr(new console_iface)
    //iface_ptr(new console_iface_2)
    iface_ptr(new file_iface)
    {}
    
    void execute(void);
    
  protected:
    kernel_pointer kernel_ptr;
    kernel_iface_pointer iface_ptr;
};

#endif // CONSOLE_PCKG_H
