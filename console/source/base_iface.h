#if !defined BASE_IFACE_H
#define BASE_IFACE_H

#include <boost/shared_ptr.hpp>

class base_iface
{
  public:
    virtual void notify(void) = 0;
};

typedef boost::shared_ptr<base_iface> base_iface_pointer;

#endif // BASE_IFACE_H
