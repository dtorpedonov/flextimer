#include <string>
#include <iostream>

#include "file_iface.h"
#include "kernel.h"

using namespace std;

void file_iface::notify(void)
{
  kernel::timer_state state = this->kernel_ptr->get_state();
  
  if (state == kernel::INIT) { 
    //this->display_header(); 
    
    int counter = this->get_timer_counter();
    this->kernel_ptr->set_timer(counter);
    
    counter = this->get_repeat();
    this->kernel_ptr->set_repeat(counter);
    
    bool check = this->check_another();
    this->kernel_ptr->set_another(check);
  }
  
  if (state == kernel::PROGRESS) {
    this->display_counter( this->kernel_ptr->get_timer_time());
  }
  
  if (state == kernel::FINISH) {
    this->display_finish();
  }
}


void file_iface::display_header()
{
  cout << "start" << endl; 

}

void file_iface::display_counter(time_interval actual_time)
{ cout << '\r' << actual_time << flush; }

void file_iface::display_finish(void)
{ cout << endl << "finish" << endl; }

int file_iface::get_timer_counter(void)
{
  string counter;
  this->timetable >> counter;
  return atoi(counter.c_str());
}

int file_iface::get_repeat(void)
{
  string counter;
  this->timetable >> counter;
  return atoi(counter.c_str());
}

bool file_iface::check_another(void)
{
  char empty;
  this->timetable.get(empty);
  if (this->timetable.eof()) { return false; }
  this->timetable.unget();
  return true;
}
