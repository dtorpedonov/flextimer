#if !defined BASE_TIMER_H
#define BASE_TIMER_H

class base_timer 
{
  base_timer(void) {}

  protected:

  virtual void add() = 0;
  
  virtual void execute() = 0;
};

#endif // BASE_TIMER
