#include <iostream>

#include "console_iface.h"
#include "kernel.h"

using namespace std;

void console_iface::notify(void)
{
  kernel::timer_state state = this->kernel_ptr->get_state();
  
  if (state == kernel::PROGRESS) {
    this->display_counter( this->kernel_ptr->get_timer_time());
  }
  
  if (state == kernel::FINISH) {
    this->display_finish();
  }
}

void console_iface::display_counter(time_interval actual_time)
{ cout << actual_time << endl; }

void console_iface::display_finish(void)
{ cout << "finish" << endl; }
