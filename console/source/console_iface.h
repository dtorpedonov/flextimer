#if !defined CONSOLE_INTERFACE_H
#define CONSOLE_INTERFACE_H

#include <boost/shared_ptr.hpp>

#include "kernel_iface.h"
#include "kernel.h"

using namespace std;

class console_iface : public kernel_iface
{
  public:
    console_iface(void) {}

    virtual void notify(void);
    
  private:
    void display_counter(time_interval actual_time);
    
    void display_finish(void);
};

#endif // CONSOLE_INTERFACE_H
