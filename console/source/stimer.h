#if !defined SINGLE_TIMER_H
#define SINGLE_TIMER_H

#include <boost/chrono.hpp>
#include <boost/shared_ptr.hpp>
#include <cmath>

typedef boost::chrono::duration<double> time_interval; // TODO: move all repeated typedefs to one place
typedef boost::chrono::steady_clock::time_point time_point; 


class single_timer
{
  public:
    single_timer(void) : current_sec(), start_time(), max_counter(5), // TODO: why 5 ?
    iteration(0), num_of_repeat(3) {}
    
    time_interval get_time(void) { return current_sec; }
    bool check_next_iteration(void) { return iteration < num_of_repeat; }
    
    void set_timer(int max_counter_) { max_counter = max_counter_; }
    void set_repeat(int repeat_) { num_of_repeat = repeat_; }
    
    void start();
    bool is_finished();
    
  protected:
    time_interval current_sec;
    time_point start_time;
    
    int max_counter;
    
    int iteration;
    int num_of_repeat;
    
    time_point get_now(void);
    time_interval to_seconds(long);
};

typedef boost::shared_ptr<single_timer> single_timer_pointer;

#endif // SINGLE_TIMER_H
