#if !defined FILE_INTERFACE_H
#define FILE_INTERFACE_H

#include <boost/shared_ptr.hpp>
#include <fstream>

#include "kernel_iface.h"
#include "kernel.h"


class file_iface : public kernel_iface
{
  public:
    file_iface(void) : timetable("../data/timetable.txt") {
      display_header();
    }

    virtual void notify(void);
    
  private:
    void display_header(void);
    void display_counter(time_interval actual_time);
    void display_finish(void);
    int  get_timer_counter(void);
    int  get_repeat(void);
    bool check_another(void);
    
    std::fstream timetable;
};

#endif // FILE_INTERFACE_2_H
